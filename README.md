# geodataService

This application was generated using JHipster 6.10.3, you can find documentation and help at [https://www.jhipster.tech/documentation-archive/v6.10.3](https://www.jhipster.tech/documentation-archive/v6.10.3).

This is a "microservice" application intended to be part of a microservice architecture, please refer to the [Doing microservices with JHipster][] page of the documentation for more information.

This application is configured for Service Discovery and Configuration with the JHipster-Registry. On launch, it will refuse to start if it is not able to connect to the JHipster-Registry at [http://localhost:8761](http://localhost:8761). For more information, read our documentation on [Service Discovery and Configuration with the JHipster-Registry][].

## Development

To start your application in the dev profile, run:

```
./gradlew
```

For further instructions on how to develop with JHipster, have a look at [Using JHipster in development][].

## Profiles
Available profiles:
- dev
- prod
- swagger
- livorno : allows security free  calls to all /api resources

### Supported prod environment variables:
Following variables can be supplied to the prod environment, for example in docker-compose yml file
- DATASOURCE_URL
- DB_USER
- DB_PWD
- LIQUIBASE_CONTEXTS
- DB_SCHEMA
- JPA_DATABASE_PLATFORM
- EUREKA_CLIENT_ENABLED (optional, default value is true)

**Example 1: geodataService as standalone service**

This configuratin will result in geodataService being run against inmemory H2 database without Eureka service registry and no Spring cloud config:

```
version: '2'
services:
  geodataapp-app:
    network_mode: 'host'
    image: ag04/geodata-service-jwt:latest
    environment:
      _JAVA_OPTIONS: '-Xmx512m -Xms256m'
      SPRING_PROFILES_ACTIVE: 'prod,swagger,livorno'
      MANAGEMENT_METRICS_EXPORT_PROMETHEUS_ENABLED: 'true'
      JHIPSTER_SLEEP: '2' # gives time for other services to boot before the application
      DATASOURCE_URL: 'jdbc:h2:mem:geodataservice;DB_CLOSE_DELAY=-1'
      DB_USER: 'sa'
      DB_PWD: 'password'
      LIQUIBASE_CONTEXTS: 'prod'
      DB_SCHEMA: 'public'
      JPA_DATABASE_PLATFORM: 'io.github.jhipster.domain.util.FixedH2Dialect'
      EUREKA_CLIENT_ENABLED: false
      SPRING_CLOUD_CONFIG_ENABLED: 'false'
    ports:
      - 8090:8090
```

## Building for production

### Packaging as jar

To build the final jar and optimize the geodataService application for production, run:

```
./gradlew -Pprod clean bootJar
```

To ensure everything worked, run:

```
java -jar build/libs/*.jar
```

Refer to [Using JHipster in production][] for more details.

### Packaging as war

To package your application as a war in order to deploy it to an application server, run:

```
./gradlew -Pprod -Pwar clean bootWar
```

## Testing

To launch your application's tests, run:

```
./gradlew test integrationTest jacocoTestReport
```

For more information, refer to the [Running tests page][].

### Code quality

Sonar is used to analyse code quality. You can start a local Sonar server (accessible on http://localhost:9001) with:

```
docker-compose -f src/main/docker/sonar.yml up -d
```

You can run a Sonar analysis with using the [sonar-scanner](https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner) or by using the gradle plugin.

Then, run a Sonar analysis:

```
./gradlew -Pprod clean check jacocoTestReport sonarqube
```

For more information, refer to the [Code quality page][].

## Using Docker to simplify development (optional)

You can use Docker to improve your JHipster development experience. A number of docker-compose configuration are available in the [src/main/docker](src/main/docker) folder to launch required third party services.

For example, to start a postgresql database in a docker container, run:

```
docker-compose -f src/main/docker/postgresql.yml up -d
```

To stop it and remove the container, run:

```
docker-compose -f src/main/docker/postgresql.yml down
```

You can also fully dockerize your application and all the services that it depends on.
To achieve this, first build a docker image of your app by running:

```
./gradlew bootJar -Pprod jibDockerBuild
```

Then run:

```
docker-compose -f src/main/docker/app.yml up -d
```

For more information refer to [Using Docker and Docker-Compose][], this page also contains information on the docker-compose sub-generator (`jhipster docker-compose`), which is able to generate docker configurations for one or several JHipster applications.

## Building docker images

To build a Docker image of **geodata-service-jwt** application using Jib connecting to the local Docker daemon:

```sh
./gradlew bootJar -Pprod jibDockerBuild
```
This will build a docker image with the name "ag04/geodata-service-jwt" and with version equal to the current project version.

Optionally you can also tag this image as "latest" with the following command:

```sh
docker tag ag04/geodata-service-jwt:${projectVersion} ag04/geodata-service-jwt:latest
```

To build a Docker image of your application without Docker and push it directly into your Docker registry (by default docker.io), run:

```sh
./gradlew jib -Pprod bootJar
```

**Important: Do not forget to perform `docker login` before issuing this command, or alternativelly, configure access token on your client machine.**


Gradle build script allows for the following project variables to be passed to the build:

* dockerRegistryUrl - url of the target docker registry (ie. AWS or Azure custom registry)
* imageName - custom image name
* imageVersion - custom image version (ie. latest)
* jib.to.auth.username - username to be used to authenticate to custom registry
* jib.to.auth.password - passwod to be used to authenticate to custom registry

This can, for example, be used in CI/CD scripts by mapping preconfigured envionment variables to the project properties, as in the snippet bellow:

```sh
./gradlew jib -Pprod -PdockerRegistryUrl=$REGISTRY_URL -PimageName=jhlab/geodata-app -PimageVersion=latest -Djib.to.auth.username=$REGISTRY_USER -Djib.to.auth.password=$REGISTRY_PWD
```

## Continuous Integration (optional)

To configure CI for your project, run the ci-cd sub-generator (`jhipster ci-cd`), this will let you generate configuration files for a number of Continuous Integration systems. Consult the [Setting up Continuous Integration][] page for more information.

[jhipster homepage and latest documentation]: https://www.jhipster.tech
[jhipster 6.10.3 archive]: https://www.jhipster.tech/documentation-archive/v6.10.3
[doing microservices with jhipster]: https://www.jhipster.tech/documentation-archive/v6.10.3/microservices-architecture/
[using jhipster in development]: https://www.jhipster.tech/documentation-archive/v6.10.3/development/
[service discovery and configuration with the jhipster-registry]: https://www.jhipster.tech/documentation-archive/v6.10.3/microservices-architecture/#jhipster-registry
[using docker and docker-compose]: https://www.jhipster.tech/documentation-archive/v6.10.3/docker-compose
[using jhipster in production]: https://www.jhipster.tech/documentation-archive/v6.10.3/production/
[running tests page]: https://www.jhipster.tech/documentation-archive/v6.10.3/running-tests/
[code quality page]: https://www.jhipster.tech/documentation-archive/v6.10.3/code-quality/
[setting up continuous integration]: https://www.jhipster.tech/documentation-archive/v6.10.3/setting-up-ci/
